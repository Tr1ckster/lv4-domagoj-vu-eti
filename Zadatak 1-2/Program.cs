﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("csv.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] res = adapter.CalculateAveragePerColumn(data);
            foreach (double num in res)
            {
                Console.WriteLine(num);
            }
        }
    }
}

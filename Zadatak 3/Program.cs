﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> rentables = new List<IRentable>();
            Book nBook = new Book("Hobbit");
            Video nVideo = new Video("LOTR");
            rentables.Add(nBook);
            rentables.Add(nVideo);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(rentables);
            printer.PrintTotalPrice(rentables);
        }
    }
}
